﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public class Match
    {
        public int Id { get; set; }
        public City CityMach { get; set; }
        public Player Player1 { get; set; }
        public Player Player2 { get; set; }
        public enum ResultType { Draw, Player1win, Player2win }
        public ResultType Result { get; set; }

        public Match(int id, City cityMach, Player player1, Player player2, ResultType result)
        {
            Id = id;
            CityMach = cityMach ?? throw new ArgumentNullException(nameof(cityMach));
            Player1 = player1 ?? throw new ArgumentNullException(nameof(player1));
            Player2 = player2 ?? throw new ArgumentNullException(nameof(player2));
            Result = result;
        }

        public static List<Match> ReadCSV(string path, Dictionary<int, City> cities, Dictionary<int, Player> players)
        {
            List<Match> readedCities = new List<Match>();

            // Read the csv
            var reader = new System.IO.StreamReader(path);

            // discard header names
            reader.ReadLine();

            string line;
            while (!reader.EndOfStream)
            {
                line = reader.ReadLine();
                string[] values = line.Split(';');


                // Create a match with direct references to the objects
                Match match = new Match(
                    int.Parse(values[0]),
                    cities[int.Parse(values[1])],
                    players[int.Parse(values[2])],
                    players[int.Parse(values[3])],
                    values[4].Equals("Draw") ? ResultType.Draw :
                    values[4].Equals("Player1") ? ResultType.Player1win :
                    ResultType.Player2win);


                readedCities.Add(match);
            }

            reader.Close();

            return readedCities;
        }
    }
}
