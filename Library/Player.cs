﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public class Player
    {
        public string Name { get; set; }

        public Player(string name)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
        }

        /// <summary>
        /// Obtiene un diccionario con los objetos 
        /// ciudad mapeados con su ID
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static Dictionary<int, Player> ReadCSV(string path)
        {
            Dictionary<int, Player> readedCities = new Dictionary<int, Player>();

            // Read the csv
            var reader = new System.IO.StreamReader(path);

            // discard header names
            reader.ReadLine();

            string line;
            while (!reader.EndOfStream)
            {
                line = reader.ReadLine();
                string[] values = line.Split(';');

                // Create dictionary entry with key=ID and value=Player
                readedCities.Add(int.Parse(values[0]), new Player(values[1]));

            }

            reader.Close();

            return readedCities;
        }
    }
}
