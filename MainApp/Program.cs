﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
// Import library in compile time
using Library;

namespace MainApp
{
    class Program
    {
        static void Main(string[] args)
        {
            // Load data from csv files
            var cities = Library.City.ReadCSV(@"../cities.csv");
            var players = Library.Player.ReadCSV(@"../players.csv");
            var games = Library.Match.ReadCSV(@"../games.csv", cities, players);

            #region city with more maches
            // What is the city with more matches ? 
            var moreMachesCity = games
                .GroupBy(game => game.CityMach)
                .OrderByDescending(city => city.Count())
                .Select(c => c.Key.Name)
                .First();

            Console.WriteLine("What is the city with more matches?\t" + moreMachesCity);
            #endregion

            #region city with more drawn maches
            // What is the city or cities with more drawn matches ? 
            var moreDrawnMatches = games
                .Where(g => g.Result.Equals(Match.ResultType.Draw))
                .GroupBy(game => game.CityMach)
                .OrderByDescending(city => city.Count())
                .Take(1)
                .First()
                .First()
                .CityMach.Name;
            Console.WriteLine("What is the city or cities with more drawn matches?\t" + moreDrawnMatches);
            #endregion

            #region player with best win ratio
            // Compute the player with best win ratio 

            // Victorias
            var winsPlayer1 = games
                .Where(g => g.Result == Match.ResultType.Player1win)
                .GroupBy(g => g.Player1)    // Agrupamos por jugador1
                .Select(g => new { player = g.Key.Name, wins = g.Count() });

            var winsPlayer2 = games
                .Where(g => g.Result == Match.ResultType.Player2win)
                .GroupBy(g => g.Player2)    // Agrupamos por jugador2
                .Select(g => new { player = g.Key.Name, wins = g.Count() });

            // Derrotas
            var losesPlayer1 = games
                .Where(g => g.Result == Match.ResultType.Player2win)
                .GroupBy(g => g.Player1)    // Agrupamos por jugador1
                .Select(g => new { player = g.Key.Name, loses = g.Count() });

            var losesPlayer2 = games
                .Where(g => g.Result == Match.ResultType.Player1win)
                .GroupBy(g => g.Player2)    // Agrupamos por jugador2
                .Select(g => new { player = g.Key.Name, loses = g.Count() });

            // Total Victorias
            var victories = winsPlayer1
                .Concat(winsPlayer2)
                // Concatenamos todos los resultados
                .GroupBy(p => p.player)     // Los agrupamos por jugador
                .Select(p => new { player = p.Key, wins = p.Sum(ply => ply.wins) });

            // Total derrotas
            var loses = losesPlayer1
                .Concat(losesPlayer2)
                // Concatenamos todos los resultados
                .GroupBy(p => p.player)     // Los agrupamos por jugador
                .Select(p => new { player = p.Key, loses = p.Sum(ply => ply.loses) });

            var ratioWinLoose = victories
                .Join(loses,
                w => w.player,
                l => l.player,
                (w, l) => new { name = w.player, ratio = (double)w.wins / (double)l.loses }); // Convertemos a double para no perder decimales

            // Consideramos el ratio como victorias/derrotas
            var bestWinRatio = ratioWinLoose
                .OrderByDescending(r => r.ratio)
                .First();

            Console.WriteLine("Compute the player with best win ratio:\t" + bestWinRatio.name + ", ratio: " + bestWinRatio.ratio + " wins/lose");
            #endregion

            #region classification
            // Compute the classification taking into account wined matches as 1 point, draw as 0.5.
            const double win = 1.0, draw = 0.5;

            // Victorias
            var winPlayer1 = games
                .Where(g => g.Result == Match.ResultType.Player1win)
                .GroupBy(g => g.Player1)    // Agrupamos por jugador1
                .Select(g => new { player = g.Key.Name, points = g.Count() * win });    // Computamos las  victorias como 1 punto
            var winPlayer2 = games
                .Where(g => g.Result == Match.ResultType.Player2win)
                .GroupBy(g => g.Player2)
                .Select(g => new { player = g.Key.Name, points = g.Count() * win });
            // Empates
            var drawPlayer1 = games
                .Where(g => g.Result == Match.ResultType.Draw)
                .GroupBy(g => g.Player1)
                .Select(g => new { player = g.Key.Name, points = g.Count() * draw });  // Computamos los empates como la mitad de puntos
            var drawPlayer2 = games
                .Where(g => g.Result == Match.ResultType.Draw)
                .GroupBy(g => g.Player2)
                .Select(g => new { player = g.Key.Name, points = g.Count() * draw });
            // Lista de puntuaciones
            var classification = winPlayer1
                .Concat(winPlayer2)
                .Concat(drawPlayer1)
                .Concat(drawPlayer2)    // Concatenamos todos los resultados
                .GroupBy(p => p.player) // Los agrupamos por jugador
                .Select(p => new { player = p.Key, points = p.Sum(pt => pt.points) })  // Obtenemos el jugador y la suma de todas sus puntuaciones
                .OrderByDescending(p => p.points);

            Console.WriteLine("Classification ranking");
            Console.WriteLine("Points\t|\tPlayer");
            Console.WriteLine("---------------------------");
            foreach (var clasified in classification)
            {
                Console.WriteLine(clasified.points + "\t|\t" + clasified.player);
            }
            #endregion

            #region player with more matches winned
            // Compute the player with more matches winned
            var moreMachesWined = victories
                .OrderByDescending(v => v.wins)
                .First();

            Console.WriteLine("Player with more maches winned:\t" + moreMachesWined.player + "wins: " + moreMachesWined.wins);
            #endregion

            #region classification for each city
            // Compute the classification for each city
            var winPlayer1City = games
                .Where(g => g.Result == Match.ResultType.Player1win)
                .Select(g => new { player = g.Player1, points = win, city = g.CityMach });

            var winPlayer2City = games
                .Where(g => g.Result == Match.ResultType.Player2win)
                .Select(g => new { player = g.Player2, points = win, city = g.CityMach });

            var drawPlayer1City = games
                .Where(g => g.Result == Match.ResultType.Draw)
                .Select(g => new { player = g.Player1, points = draw, city = g.CityMach });

            var drawPlayer2City = games
                .Where(g => g.Result == Match.ResultType.Draw)
                .Select(g => new { player = g.Player2, points = draw, city = g.CityMach });

            var listOfCityScores = winPlayer1City
                .Concat(winPlayer2City)
                .Concat(drawPlayer1City)
                .Concat(drawPlayer2City);

            var cityScores = listOfCityScores
                .GroupBy(s => new { s.city, s.player })
                .Select(c => new { city = c.Key.city.Name, player = c.Key.player.Name, score = c.Sum(sc => sc.points) })
                .OrderByDescending(c => c.score);

            var cityScoresCityGroups = cityScores
                .GroupBy(c => c.city);

            Console.WriteLine("Classification for each city");
            foreach (var city in cityScoresCityGroups)
            {
                Console.WriteLine("\n" + city.Key);
                Console.WriteLine("---------------------------------");
                foreach (var player in city)
                {
                    Console.WriteLine(" " + player.score.ToString().PadRight(5) + "\t" + player.player);
                }
            }

            #endregion

            #region worst streak
            // Compute the worst streak
            const bool wined = true, losed = false;

            var Player1Streak = games
                .Select(g => new { id = g.Id, player = g.Player1.Name, result = g.Result == Match.ResultType.Player1win ? wined : losed });

            var Player2Streak = games
                .Select(g => new { id = g.Id, player = g.Player2.Name, result = g.Result == Match.ResultType.Player2win ? wined : losed });

            var listOfAllGames = Player1Streak
                .Concat(Player2Streak)
                .OrderBy(l => l.id)
                .OrderBy(p => p.player)
                .ToList();  // Si no lo utilizamos como lista tarda una barbaridad en acabar

            // Algoritmo a recrear en linq
            //int num = 0;
            //int max = 0;
            //string best;
            //string current = losesStrike.ElementAt(0).player;
            //for (int i = 1; i < losesStrike.Count(); i++)
            //{
            //    if (current == losesStrike.ElementAt(i - 1).player && losesStrike.ElementAt(i).result == losed)
            //    {
            //        num++;
            //    }
            //    else
            //    {
            //        if (num > max)
            //        {
            //            max = num;
            //            best = current;
            //        }
            //        current = losesStrike.ElementAt(i).player;
            //        num = 0;
            //    }
            //}

            // Variable para asignar un valor a cada strike de derrotas
            int labelStrike = 0;

            var strikes = listOfAllGames
                .Select((item, i) => new
                {
                    Item = item,
                    strike =
                    i == 0 ? labelStrike :
                    item.player == listOfAllGames.ElementAt(i - 1).player && item.result == losed ? labelStrike : ++labelStrike
                });

            var firstGroup = strikes
                .Where(sr => sr.Item.result == losed)
                .GroupBy(s => s.strike)
                .OrderByDescending(s => s.Count())
                .First();


            Console.WriteLine("Compute the worst streak:\tMade by "
                + firstGroup.First().Item.player + " and its a streak of "
                + firstGroup.Count() + " losed maches");
            #endregion

            #region win 5 or more consecutive maches
            // Compute the player or players that has win 5 or more consecutive matches

            int labelWinStrike = 0;

            var winStrikes = listOfAllGames
                .Select((item, i) => new
                {
                    Item = item,
                    strike =
                    i == 0 ? labelStrike :
                    item.player == listOfAllGames.ElementAt(i - 1).player && item.result == wined ? labelWinStrike : ++labelWinStrike
                });

            var MoreThan5MachesWinedStrike = winStrikes
                .Where(sr => sr.Item.result == wined)
                .Select(s => new { s.Item.player, s.strike })
                .GroupBy(s => s.strike)
                .Where(g => g.Count() >= 5) // Descartamos los que no hayan ganado mas de 5
                .OrderByDescending(s => s.Count());

            // Ahora debemos obtener unicamente los jugadores sin repetir que tienen strikes de mas de 5 victorias
            // ya que ahora tendremos guardados los strikes de todos los jugadores

            var MoreThan5Players = MoreThan5MachesWinedStrike
                .Select(g => g.First()); // Nos quedamos solamente con el nombre de cada jugador y su racha

            // Eliminamos las rachas del mismo jugador
            var rachasDeMasDe5 = MoreThan5Players
                .GroupBy(r => r.player)
                .Select(g => g.First());

            Console.WriteLine("\nCompute the player or players that has win 5 or more consecutive matches:");
            Console.WriteLine("(**more than 5** excluding players who made just 5 wins in a row)");
            foreach (var r in rachasDeMasDe5)
            {
                Console.WriteLine(r.player);
            }
            #endregion

            #region unbeaten player of each city
            // Compute for each city the unbeaten players.

            var Player1Unbeaten = games
                .Select(g => new { id = g.Id, player = g.Player1.Name, city = g.CityMach.Name, result = g.Result == Match.ResultType.Player1win ? wined : losed });

            var Player2Unbeaten = games
                .Select(g => new { id = g.Id, player = g.Player2.Name, city = g.CityMach.Name, result = g.Result == Match.ResultType.Player2win ? wined : losed });

            var AllGames = Player1Unbeaten
                .Concat(Player2Unbeaten)
                .OrderBy(l => l.id)
                .OrderBy(p => p.player)
                .ToList();

            // Consultamos los jugadores que han perdido en alguna ciudad
            var losedGames = AllGames
                .Where(g => g.result == losed)
                .Select(g => new { g.player, g.city });

            //Eliminamos los jugadores que han sido derrotados en una ciudad concreta
            AllGames.RemoveAll(g => losedGames.Contains(new { g.player, g.city }));

            var playersByCity = AllGames
                .Select(p => new { p.player, p.city})
                .Distinct()
                .GroupBy(g => g.city);

            Console.WriteLine("\nCompute for each city the unbeaten players:");
            foreach (var city in playersByCity)
            {
                Console.WriteLine("\nUnbeaten players in " + city.Key);
                foreach (var pl in city)
                {
                    Console.WriteLine("-" + pl.player);
                }
            }
                


            #endregion


            Console.Read();
        }
    }
}
